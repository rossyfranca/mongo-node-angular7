import { connect } from "net";

var mysql = require('mysql');
var config = require('./config')

export default class DB {


    async findAll() {
        return new Promise((resolve, reject) => {
            const conn = mysql.createConnection({
                host: config.host,
                user: config.user,
                password: config.password,
                database: config.database
            });
            conn.query('SELECT * FROM produtos',
                (err, result, fields) => {
                    if (err) {
                        console.log(err)
                        reject(err);
                        conn.end();
                    }
                    else {
                        resolve(result)
                        conn.end();
                        console.log('executou');
                    }
                });
        });
    }

    async insert(values) {
        return new Promise((resolve, reject) => {
            const conn = mysql.createConnection({
                host: config.host,
                user: config.user,
                password: config.password,
                database: config.database
            });

            conn.query(`INSERT INTO produtos SET ?,?`, `${values}`,
                (err, result, fields) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                        conn.end();
                    }
                    else {
                        resolve(result)
                        conn.end();
                        console.log('Produto inserido')
                    }
                });
        })
    }
}










// var con = mysql.createConnection({
//     host: config.host,
//     user: config.user,
//     password: config.password
// });

// con.connect((err: any) => {
//     if (err) throw err;
//     console.log('Connected');
//     con.query("CREATE DATABASE  IF NOT EXISTS bardb", (err, result) => {
//         if (err) throw err;
//         console.log("Database created!");
//     });
// });



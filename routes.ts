import { ProdutosController } from "./controllers/produtos.service";
const values = [
    [10, 'teste'],
    [2, 'teste2']
]
export class Routes {


    public produtosCtrl: ProdutosController = new ProdutosController();


    public routes(app: any): void {
        app.route('/produtos').get(this.produtosCtrl.listar);
        app.route('/produtos').post(this.produtosCtrl.inserirProduto)
        // app.route('/produtos').post(this.produtosCtrl.inserirProduto(values))

    }

}